package mk.ukim.finki.landslides.model.enums;

public enum Luzna {
        Celna("Челна"),
        Sekundarna("Секундарна");

        public final String label;


        Luzna(String label) {
            this.label = label;
        }


}
