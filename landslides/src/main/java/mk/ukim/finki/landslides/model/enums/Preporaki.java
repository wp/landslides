package mk.ukim.finki.landslides.model.enums;

public enum Preporaki {
    RedovnoOdrzuvanje("Редовно одржување"),
    UrgentnoOdrzuvanje("Ургентно одржување"),
    Monitoring("Мониторинг"),
    Istrazuvanje("Истражување/проектирање"),
    ItnaSanacija("Итна санација");

    public final String label;

    Preporaki(String label) {
        this.label = label;
    }
}
