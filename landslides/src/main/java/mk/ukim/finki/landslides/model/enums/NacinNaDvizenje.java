package mk.ukim.finki.landslides.model.enums;

public enum NacinNaDvizenje {
        Translatorno("Транслаторно"),
        Rotaciono("Ротационо"),

        Kombinirano("Комбинирано"),
        Slozeno("Сложено");

        public final String label;

        NacinNaDvizenje(String label) {
            this.label = label;
        }
    }
