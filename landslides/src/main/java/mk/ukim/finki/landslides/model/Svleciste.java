package mk.ukim.finki.landslides.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

import mk.ukim.finki.landslides.model.enums.*;

import java.util.Date;
@Entity
public class Svleciste {
    //------------------1.Општи податоци за патот------------------
    @Id
    private Long idBroj;
    private String oznakaNaPatot;
    private String lokalnost;
    private Float koordinatiX;
    private Float KoordinatiY;
    private String stacionaza;
    private Date datumNaRegistracija;
    private Date datumNaReAktiviranje;
    private String odgovorenIstrazuvac;
    //------------------2.Општи податоци за просесот------------------
    @Enumerated(EnumType.STRING)
    private TipNaPojava tipNaPojava;
    @Enumerated(EnumType.STRING)
    private VidNaPridvizeniotMaterijal vidNaPridvizeniotMaterijal;
    @Enumerated(EnumType.STRING)
    private SodrzinaNaVoda sodrzinaNaVoda;
    @Enumerated(EnumType.STRING)
    private BrzinaNaDvizenje brzinaNaDvizenje;
    @Enumerated(EnumType.STRING)
    private Aktivnost aktivnost;
    @Enumerated(EnumType.STRING)
    private TrendNaSirenjeNaProcesot trendNaSirenjeNaProcesot;
    @Enumerated(EnumType.STRING)
    private NacinNaPojavuvanje nacinNaPojavuvanje;
    // private Image skica???????

    //------------------3.Општи податоци за теренот и положбата на трасата на патот------------------
    @Enumerated(EnumType.STRING)
    private ReljefVdolzTrasata reljefVdolzTrasata;
    private String vidNaOsnovnataKarpa;
    private String starost;
    @Enumerated(EnumType.STRING)

    private DaNe daliERaspadnataKarpa;
    private Float debelinaNaRaspadnataKarpa;
    @Enumerated(EnumType.STRING)
    private StepenNaRaspadnatost stepenNaRaspadnatost;
    @Enumerated(EnumType.STRING)
    private Vodoteci vodoteci;
    private String ostanatoHidrologija;
    private String hidroGeoloskaFunkcija;
    private String tipNaIzdani;
    @Enumerated(EnumType.STRING)
    private Pojavi pojavi;
    //------------------4.Детален опис на појавата------------------
    @Enumerated(EnumType.STRING)
    private Forma forma;
    private Float geoDolzina;
    private Float geoSirina;
    private Float geoDlabina;
    private Float geoPovrsina;
    private Float geoVolumen;
    private Long geoNaklonOdVrvDoNozica;
    private Float geoVisinaOdVrvDoNozica;
    @Enumerated(EnumType.STRING)
    private NacinNaDvizenje nacinNaDvizenje;
    @Enumerated(EnumType.STRING)
    private Luzna luzna;
    private Float skokLuzna;
    @Enumerated(EnumType.STRING)
    private PriciniZaNestabilnosta priciniZaNestabilnosta;
    private Long brDominantiFamiliiNaPuknatini;
    @Enumerated(EnumType.STRING)

    private DaNe daliImaPoedinecniPuknatini;
    @Enumerated(EnumType.STRING)
    private OrientacijaNaPuknatinite orientacijaNaPuknatinite;
    @Enumerated(EnumType.STRING)
    private MaxGoleminaNaIzdvoenBlok maxGoleminaNaIzdvoenBlok;
    @Enumerated(EnumType.STRING)
    private MinGoleminaNaIzdvoenBlok minGoleminaNaIzdvoenBlok;
    @Enumerated(EnumType.STRING)
    private Mehanizam mehanizam;
    private Float visinaDoNajvisokaZonaNaOdvojuvanje;
    private Long bazicenAgolNaTrienje;
    @Enumerated(EnumType.STRING)
    private StepenNaIstrazenost stepenNaIstrazenost;
    private String tehnickaDokumentacija;
    @Enumerated(EnumType.STRING)
    private NeposrednaPricinaZaPocnuvanjeNaProces neposrednaPricinaZaPocnuvanjeNaProces;
    //------------------5.Загрозеност------------------
    @Enumerated(EnumType.STRING)
    private PolozbaNaTrasata polozbaNaTrasata;
    @Enumerated(EnumType.STRING)

    private DaNe daliEZagrozenPatot;
    @Enumerated(EnumType.STRING)

    private DaNe daliEPrekinatSoobrakjajot;
    @Enumerated(EnumType.STRING)
    private ZagrozenostNaPatotSoPojavata zagrozenostNaPatotSoPojavata;
    @Enumerated(EnumType.STRING)
    private PravecKrivinaElementiPat pravecKrivinaElementiPat;
    @Enumerated(EnumType.STRING)
    private ElemNaPatVoZonaNaPojava elemNaPatVoZonaNaPojava;
    @Enumerated(EnumType.STRING)
    private Preporaki preporaki;
    private String dopolnitelniNapomeniIPreporaki;


//    public Svleciste(){
//        brDominantiFamiliiNaPuknatini.
//    }







}
