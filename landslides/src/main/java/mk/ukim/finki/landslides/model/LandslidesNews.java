package mk.ukim.finki.landslides.model;

import javax.persistence.*;


import lombok.Data;
import mk.ukim.finki.landslides.model.Location;

import java.time.LocalDate;
import java.util.List;
import java.time.LocalDate;

@Data
@Entity
public class LandslidesNews {
    @Id
    @Column(name = "id")
    private String id;
    @ManyToMany
    private List<Location> location;

    private String title;

    @Column(columnDefinition = "TEXT")
    private String snippet;

    private String source;

    private LocalDate when;

    public LandslidesNews(){

    }

    public LandslidesNews(String id, String title, List<Location> location, String snippet, String source, LocalDate when){
        this.id = id;
        this.title = title;
        this.location = location;
        this.snippet = snippet;
        this.source = source;
        this.when = when;
    }
}
