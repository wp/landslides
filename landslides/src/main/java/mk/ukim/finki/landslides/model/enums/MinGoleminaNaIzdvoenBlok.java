package mk.ukim.finki.landslides.model.enums;

public enum MinGoleminaNaIzdvoenBlok {
    Pod1("<1"),
    Od1do8("1-8"),
    Nad8(">8");

    public final String label;


    MinGoleminaNaIzdvoenBlok(String label) {
        this.label = label;
    }

}
