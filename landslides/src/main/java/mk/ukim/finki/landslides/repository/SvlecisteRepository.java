package mk.ukim.finki.landslides.repository;

import mk.ukim.finki.landslides.model.Svleciste;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SvlecisteRepository extends JpaRepository<Svleciste,Long> {
}
