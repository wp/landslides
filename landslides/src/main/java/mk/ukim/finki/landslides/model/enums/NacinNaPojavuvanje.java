package mk.ukim.finki.landslides.model.enums;

public enum NacinNaPojavuvanje {
    Poedinecno("Поединечно"),
    Sukcesivno("Сукцесивно"),
    Povekjekratno("Повеќекратно"),
    Meshovito("Мешовито"),
    Kompleksno("Комплексно");

    public final String label;

    NacinNaPojavuvanje(String label) {
        this.label = label;
    }
}
