package mk.ukim.finki.landslides.web;

import mk.ukim.finki.landslides.model.Svleciste;
import mk.ukim.finki.landslides.model.enums.*;
import mk.ukim.finki.landslides.service.SvlecisteService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping
public class SvlecisteController {

    private final SvlecisteService svlecisteService;

    public SvlecisteController(SvlecisteService svlecisteService) {
        this.svlecisteService = svlecisteService;
    }

    @GetMapping({"/","/landslides"})
    public String showList(Model model){
        List<Svleciste>svlecista=svlecisteService.listAll();
        model.addAttribute("svlecista",svlecista);
      return null;
    }

    @GetMapping("/landslides/add")
    public String showAdd(Model model){
        model.addAttribute("Aktivnost", Aktivnost.values());
        model.addAttribute("BrzinaNaDvizenje", BrzinaNaDvizenje.values());
        model.addAttribute("ElemNaPatVoZonaNaPojava", ElemNaPatVoZonaNaPojava.values());
        model.addAttribute("Forma", Forma.values());
        model.addAttribute("Luzna", Luzna.values());
        model.addAttribute("MaxGoleminaNaIzdvoenBlok", MaxGoleminaNaIzdvoenBlok.values());
        model.addAttribute("Mehanizam", Mehanizam.values());
        model.addAttribute("MinGoleminaNaIzdvoenBlok", MinGoleminaNaIzdvoenBlok.values());
        model.addAttribute("NacinNaDvizenje", NacinNaDvizenje.values());
        model.addAttribute("NacinNaPojavuvanje", NacinNaPojavuvanje.values());
        model.addAttribute("NeposrednaPricinaZaPocnuvanjeNaProces", NeposrednaPricinaZaPocnuvanjeNaProces.values());
        model.addAttribute("OrientacijaNaPuknatinite", OrientacijaNaPuknatinite.values());
        model.addAttribute("Pojavi", Pojavi.values());
        model.addAttribute("PolozbaNaTrasata", PolozbaNaTrasata.values());
        model.addAttribute("PravecKrivinaElementiPat", PravecKrivinaElementiPat.values());
        model.addAttribute("Preporaki", Preporaki.values());
        model.addAttribute("PriciniZaNestabilnosta", PriciniZaNestabilnosta.values());
        model.addAttribute("ReljefVdolzTrasata", ReljefVdolzTrasata.values());
        model.addAttribute("SodrzinaNaVoda", SodrzinaNaVoda.values());
        model.addAttribute("StepenNaIstrazenost", StepenNaIstrazenost.values());
        model.addAttribute("StepenNaRaspadnatost", StepenNaRaspadnatost.values());
        model.addAttribute("TipNaPojava", TipNaPojava.values());
        model.addAttribute("Trasa", Trasa.values());
        model.addAttribute("TrendNaSirenjeNaProcesot", TrendNaSirenjeNaProcesot.values());
        model.addAttribute("VidNaPridvizeniotMaterijal", VidNaPridvizeniotMaterijal.values());
        model.addAttribute("Vodoteci", Vodoteci.values());
        model.addAttribute("ZagrozenostNaPatotSoPojavata", ZagrozenostNaPatotSoPojavata.values());
        model.addAttribute("DaNe", DaNe.values());
        return "form";
    }
    @PostMapping("/landslides")
    public String create(){
        svlecisteService.create();
        return "redirect:/landslides";
    }

    @PostMapping("/landslides/{id}/delete")
    public String delete(@PathVariable Long id){
        this.svlecisteService.delete(id);
        return "redirect:/landslides";
    }
}
