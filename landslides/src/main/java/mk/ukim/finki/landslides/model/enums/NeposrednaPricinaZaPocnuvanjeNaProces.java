package mk.ukim.finki.landslides.model.enums;

public enum NeposrednaPricinaZaPocnuvanjeNaProces {
    Vrnezi("Врнежи"),
    Erozija_na_nozica_na_padina("Ерозија на ножица на падина"),
    Zemjotres("Земјотрес"),
    NPV("Нагли осцилации на Ниво на подземна вода (НПВ)"),
    Neposredni_antropogeni("Непосредни антропогени"),
    Ostanati("Останати");

    public final String label;

    NeposrednaPricinaZaPocnuvanjeNaProces(String label) {
        this.label = label;
    }
}
