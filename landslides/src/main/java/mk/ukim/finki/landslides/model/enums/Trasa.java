package mk.ukim.finki.landslides.model.enums;

public enum Trasa {
    Vrvot("Траса на врвот"),
    Sredina("Траса во средина"),
    Dnoto("Траса на дното");

    public final String label;

    Trasa(String label) {
        this.label = label;
    }
}
