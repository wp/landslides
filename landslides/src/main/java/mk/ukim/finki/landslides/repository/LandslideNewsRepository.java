package mk.ukim.finki.landslides.repository;


import mk.ukim.finki.landslides.model.LandslidesNews;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

public interface LandslideNewsRepository extends JpaRepository<LandslidesNews, String> {
    Page<LandslidesNews> findByWhenBetween(LocalDate startDate, LocalDate endDate, Pageable pageable);

    Page<LandslidesNews> findByTitleContainingOrSnippetContaining(String title, String snippet, Pageable pageable);

    Page<LandslidesNews> findBySource(String source, Pageable pageable);

    @Query("SELECT ln FROM LandslidesNews ln JOIN ln.location loc WHERE lower(loc.name) = :location")
    Page<LandslidesNews> findByAllLocationName(String location, Pageable pageable);


}
