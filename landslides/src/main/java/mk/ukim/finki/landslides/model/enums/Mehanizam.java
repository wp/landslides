package mk.ukim.finki.landslides.model.enums;

public enum Mehanizam {
    Blok("Блок"),
    Klin("Клин"),
    Prevrtuvanje("Превртување");

    public final String label;

    Mehanizam(String label) {
        this.label = label;
    }
}
