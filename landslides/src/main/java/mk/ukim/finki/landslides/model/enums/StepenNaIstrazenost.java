package mk.ukim.finki.landslides.model.enums;

public enum StepenNaIstrazenost {
    Detalno("Детално"),
    Delumno("Делумно"),
    Registrirano("Регистрирано");

    public final String label;

    StepenNaIstrazenost(String label) {
        this.label = label;
    }
}
