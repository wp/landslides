package mk.ukim.finki.landslides.model.enums;

public enum ReljefVdolzTrasata {
    Greben("Гребен"),
    Dolzina("Должина"),
    Padina("Падина");

    public final String label;

    ReljefVdolzTrasata(String label) {
        this.label = label;
    }
}
