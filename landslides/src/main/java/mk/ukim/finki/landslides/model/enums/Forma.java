package mk.ukim.finki.landslides.model.enums;

public enum Forma {
    Kruzna("Кружна"),
    Elipsa("Елипса"),

    Frontalna("Фронтална"),
    Trapezasta("Трапезаста"),
    Izdolzena("Издолжена"),
    Jazicesta("Јазичеста"),
    Nepravilna("Неправилна");

    public final String label;


    Forma(String label) {
        this.label = label;
    }
}
