package mk.ukim.finki.landslides.model.enums;

public enum TipNaPojava {
    Odronuvanje("Одронување"),
    Prevrtuvanje("Превртување"),
    Lizganje("Лизгање"),
    Bocno("Бочно Ширење"),
    Tecenje("Течење"),
    Slozeno("Сложено");

    public final String label;

    TipNaPojava(String label) {
        this.label = label;
    }
}
