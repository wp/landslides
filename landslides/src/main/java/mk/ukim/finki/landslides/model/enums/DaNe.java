package mk.ukim.finki.landslides.model.enums;

public enum DaNe {
    Da("Да"),
    Ne("Не");

    public final String label;

    DaNe(String label) {
        this.label = label;
    }

}
