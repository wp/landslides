package mk.ukim.finki.landslides.service;





import mk.ukim.finki.landslides.model.LandslidesNews;
import org.springframework.data.domain.Page;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface LandslidesNewsService {
    Page<LandslidesNews> findAll(Integer pageNo, Integer pageSize, String sortBy);



    Optional<LandslidesNews> save(Map<String, Object> request);

    LandslidesNews save(LandslidesNews landslidesNews);



    Page<LandslidesNews> findByDateRange(LocalDate startDate, LocalDate endDate, Integer pageNo, Integer pageSize, String sortBy);


    Page<LandslidesNews> findByText(String searchText, Integer pageNo, Integer pageSize, String sortBy);

    Page<LandslidesNews> findBySource(String source, Integer pageNo, Integer pageSize, String sortBy);

    LandslidesNews findbyId(String id);

    LandslidesNews edit(String id, String title, List<Long> locationId,  String snippet, String source, LocalDate when);
    void deleteById(String id);

    Page<LandslidesNews> findByLocationName(String location, Integer pageNo, Integer pageSize, String sortBy);





}
