package mk.ukim.finki.landslides.model.enums;

public enum ZagrozenostNaPatotSoPojavata {
    Pogolema_kolicina("Обрушување на поголема количина на материјал врз патот"),
    Povremeno_obrusuvanje("Повремено обрушување на материјал врз патот"),

    Postojano_obrusuvanje("Постојано обрушување на материјал врз патот"),
    Tesko_ostet_kolovoz("Коловозната конструкција е тешко оштетена, со денивелација во попречен правец"),
    Malku_ostet_kolovoz("Коловозната конструкција е малку оштетена од обрушениот материјал"),
    Ne_ostet_kolovoz_neprood("Коловозната конструкција не е оштетена, само затрупана со материјал"),
    Ne_ostet_kolovoz_prood("Коловозот во зона на појавата не оштетен; можна е проодност со внимание"),
    Ostetuvanje_potporni_konstrukcii("Оштетување на потпорни конструкции во усеци и засеци");

    public final String label;


    ZagrozenostNaPatotSoPojavata(String label) {
        this.label = label;
    }
}
