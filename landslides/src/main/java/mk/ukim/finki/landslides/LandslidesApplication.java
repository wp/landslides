package mk.ukim.finki.landslides;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LandslidesApplication {

    public static void main(String[] args) {
        SpringApplication.run(LandslidesApplication.class, args);
    }

}
