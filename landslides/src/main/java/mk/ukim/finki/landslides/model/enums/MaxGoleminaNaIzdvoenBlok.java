package mk.ukim.finki.landslides.model.enums;

public enum MaxGoleminaNaIzdvoenBlok {
    Pod8("<8"),
    Od8do125("8-125"),
    Nad125(">125");

    public final String label;


    MaxGoleminaNaIzdvoenBlok(String label) {
        this.label = label;
    }
}
