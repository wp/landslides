package mk.ukim.finki.landslides.model.enums;

public enum PravecKrivinaElementiPat {
    Pravec("Правец"),
    Krivina("Кривина");

    public final String label;
    PravecKrivinaElementiPat(String label) {
        this.label = label;
    }

}
