package mk.ukim.finki.landslides.model.enums;

public enum BrzinaNaDvizenje {
    EksBavno("Екстремно бавно"),
    MnBavno("Многу бавно"),
    Bavno("Бавно"),
    Umereno("Умерено"),
    Brzo("Брзо"),
    MnBrzo("Многу брзо"),
    EksBrzo("Екстремно брзо");

    public final String label;

    BrzinaNaDvizenje(String label) {
        this.label = label;
    }

}
