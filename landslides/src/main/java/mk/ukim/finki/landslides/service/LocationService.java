package mk.ukim.finki.landslides.service;



import mk.ukim.finki.landslides.model.Location;

import java.util.List;

public interface LocationService {
    Location findById(Long id);

    List<Location> listAll();
    Location create(String name);
}
