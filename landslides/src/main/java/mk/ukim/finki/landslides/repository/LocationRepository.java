package mk.ukim.finki.landslides.repository;


import mk.ukim.finki.landslides.model.Location;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationRepository extends JpaRepository<Location, Long> {
    Page<Location> findByName(String location, Pageable pageable);
}
