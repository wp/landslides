package mk.ukim.finki.landslides.model.enums;

public enum OrientacijaNaPuknatinite {
    Povolna("Поволна"),
    Nepovolna("Неповолна");

    public final String label;


    OrientacijaNaPuknatinite(String label) {
        this.label = label;
    }
}
