package mk.ukim.finki.landslides.model.enums;

public enum VidNaPridvizeniotMaterijal {
    Karpa("Карпа"),
    Drobina("Дробина"),
    Organski("Органски материјал"),
    Atropogen("Антропоген (Вештачки) материјал"),
    Heterogen("Хетероген метеријал"),
    Glina("Глина"),
    Pesok("Песок"),
    Prasina("Прашина"),
    Cakal("Чакал");

    public final String label;

    VidNaPridvizeniotMaterijal(String label) {
        this.label = label;
    }

}
