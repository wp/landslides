package mk.ukim.finki.landslides.service.impl;


import mk.ukim.finki.landslides.model.Location;
import mk.ukim.finki.landslides.model.exceptions.LocationNotFoundException;
import mk.ukim.finki.landslides.repository.LocationRepository;
import mk.ukim.finki.landslides.service.LocationService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationServiceImpl implements LocationService {

    private final LocationRepository locationRepository;

    public LocationServiceImpl(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    @Override
    public Location findById(Long id) {
        return locationRepository.findById(id).orElseThrow(LocationNotFoundException::new);
    }

    @Override
    public List<Location> listAll() {
        return locationRepository.findAll();
    }

    @Override
    public Location create(String name) {
        return locationRepository.save(new Location(name));
    }
}
