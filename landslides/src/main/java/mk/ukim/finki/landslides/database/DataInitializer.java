package mk.ukim.finki.landslides.database;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import mk.ukim.finki.landslides.model.LandslidesNews;
import mk.ukim.finki.landslides.model.Location;
import mk.ukim.finki.landslides.repository.LandslideNewsRepository;
import mk.ukim.finki.landslides.repository.LocationRepository;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class DataInitializer {

    private final LandslideNewsRepository landslideNewsRepository;
    private final LocationRepository locationRepository;

    @PostConstruct
    public void initializeDataFromJson() {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Map<String, Map<String, Object>> jsonData = objectMapper.readValue(
                    getClass().getResourceAsStream("/data.json"),
                    new TypeReference<Map<String, Map<String, Object>>>() {});

            for (Map.Entry<String, Map<String, Object>> entry : jsonData.entrySet()) {
                String url = entry.getKey();
                Map<String, Object> data = entry.getValue();

                Map<String, Object> ner = (Map<String, Object>) data.get("ner");
                List<String> locList = (List<String>) ner.get("LOC");
                String source = (String) data.get("source");
                String title = (String) data.get("title");
                String when = (String) data.get("when");
                String snippet = (String) data.get("snippet");

                List<Location> locations = new ArrayList<>();
                if (locList != null && !locList.isEmpty()) {
                    for (String locName : locList) {
                        Location location = locationRepository.save(new Location(locName));
                        locations.add(location);
                    }
                }



                when = when != null ? when.trim() : null;

                LocalDate whenDate = null;
                if (when != null && !when.isEmpty()) {
                    DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
                    whenDate = LocalDate.parse(when, dateFormatter);
                }

                LandslidesNews existingData = landslideNewsRepository.findById(url).orElse(null);
                if (existingData == null) {
                    LandslidesNews landslidesNews = new LandslidesNews(url, title, locations, snippet, source, whenDate);
                    landslideNewsRepository.save(landslidesNews);
                }

                LandslidesNews landslidesNews = new LandslidesNews(url, title, locations, snippet, source, whenDate);
                landslideNewsRepository.save(landslidesNews);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
