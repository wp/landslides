package mk.ukim.finki.landslides.service;

import mk.ukim.finki.landslides.model.Svleciste;

import java.util.List;

public interface SvlecisteService {
    List<Svleciste> listAll();
    Svleciste findById(Long id);
    Svleciste create();
    Svleciste delete(Long id);
}
