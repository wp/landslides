package mk.ukim.finki.landslides.model.enums;

public enum Aktivnost {
    UslovnoStabilnaPadina("Условно стабилна падина"),
    Aktiven("Активен"),
    MomentalnoSmiren("Моментално смирен"),
    Reaktiviran("Реактивиран"),
    PrivremenoSmiren("Привремено смирен"),
    Smiren("Смирен"),
    Saniran("Саниран/Стабилизиран"),
    Fosilen("Фосилен");

    public final String label;

    Aktivnost(String label) {
        this.label = label;
    }
}
