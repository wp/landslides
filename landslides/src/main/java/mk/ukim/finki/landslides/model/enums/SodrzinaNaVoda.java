package mk.ukim.finki.landslides.model.enums;

public enum SodrzinaNaVoda {
    Suvo("Суво"),
    Vlazno("Влажно"),
    NaGranica("На граница на течење"),
    VoTecna("Во течна состојба");

    public final String label;

    SodrzinaNaVoda(String label) {
        this.label = label;
    }

}
