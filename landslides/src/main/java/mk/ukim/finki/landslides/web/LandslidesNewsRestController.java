package mk.ukim.finki.landslides.web;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import mk.ukim.finki.landslides.service.LandslidesNewsService;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class LandslidesNewsRestController {
    private final LandslidesNewsService landslidesNewsService;

    private String generateUniqueUrl() {
        return "url_" + System.currentTimeMillis(); // Appending timestamp to ensure uniqueness
    }

    @PostMapping("/rest")
    private void test(@RequestBody Map<String, Object> request){
        String url = generateUniqueUrl(); // Generate a unique URL here
        request.put(url, request.get(url)); // Rename the key in the request JSON
        this.landslidesNewsService.save(request);
    }






}
