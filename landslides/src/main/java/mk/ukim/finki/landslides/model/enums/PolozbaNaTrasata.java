package mk.ukim.finki.landslides.model.enums;

public enum PolozbaNaTrasata {
    Pomin_nad_chelniot_del("Патот ПОМИНУВА непосредно над челниот дел на појавата"),
    Pomin_vo_goren_del("Патот ПОМИНУВА преку појавата во нејзиниот горен дел"),
    Pomin_preku_sredina("Патот ПОМИНУВА преку средишниот дел на појавата"),
    Pomin_nozica("Патот ПОМИНУВА преку ножицата на појавата"),
    Posirok_prostor("Појавата е на поширокиот дел околу патот"),
    Gorna_strana_pat("Појавата е од горната страна на патот"),
    Dolna_strana_pat("Појавата е од долната страна на патот");

    public final String label;

    PolozbaNaTrasata(String label) {
        this.label = label;
    }
}
