package mk.ukim.finki.landslides.model.enums;

public enum ElemNaPatVoZonaNaPojava {
    Nivo_na_teren("Во ниво на терен"),
    Do3mNasip("Насип до 3.0m висина"),
    Nad3mNasip("Насип > 3.0m"),
    Do3mZasek("Засек до 3.0m"),
    Nad3mZasek("Засек > 3.0m"),
    Do3mUsek("Усек до 3.0m"),
    Nad3mUsek("Усек > 3.0m");

    public final String label;

    ElemNaPatVoZonaNaPojava(String label) {
        this.label = label;
    }
}
