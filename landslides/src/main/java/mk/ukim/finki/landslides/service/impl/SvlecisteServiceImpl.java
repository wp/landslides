package mk.ukim.finki.landslides.service.impl;

import mk.ukim.finki.landslides.model.Svleciste;
import mk.ukim.finki.landslides.repository.SvlecisteRepository;
import mk.ukim.finki.landslides.service.SvlecisteService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SvlecisteServiceImpl implements SvlecisteService {

    private final SvlecisteRepository svlecisteRepository;

    public SvlecisteServiceImpl(SvlecisteRepository svlecisteRepository) {
        this.svlecisteRepository = svlecisteRepository;
    }

    @Override
    public List<Svleciste> listAll() {
        return svlecisteRepository.findAll();
    }

    @Override
    public Svleciste findById(Long id) {
        //return svlecisteRepository.findById(id).orElseThrow(InvalidSvlecisteIdException::new);
        return svlecisteRepository.findById(id).get();
    }

    @Override
    public Svleciste create() {
        return null;
    }

    @Override
    public Svleciste delete(Long id) {
        Svleciste svleciste=svlecisteRepository.findById(id).get();
        svlecisteRepository.delete(svleciste);
        return svleciste;
    }
}
