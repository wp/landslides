package mk.ukim.finki.landslides.model.enums;

public enum StepenNaRaspadnatost {
    Zemjasta("Земјаста (почвена) маса"),
    SitnaDrobina("Ситна дробина"),
    Drobina("Дробина"),
    Blokovi("Блокови");

    public final String label;

    StepenNaRaspadnatost(String label) {
        this.label = label;
    }
}
