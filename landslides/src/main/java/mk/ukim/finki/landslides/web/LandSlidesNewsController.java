package mk.ukim.finki.landslides.web;


import mk.ukim.finki.landslides.model.LandslidesNews;
import mk.ukim.finki.landslides.model.Location;
import mk.ukim.finki.landslides.service.LandslidesNewsService;
import mk.ukim.finki.landslides.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@Controller
public class LandSlidesNewsController {

    @Autowired
    private LandslidesNewsService landslidesNewsService;

    @Autowired
    private LocationService locationService;


//    @GetMapping("/landslides")
//    public String viewLandslidesPage(
//            @RequestParam(defaultValue = "0") Integer pageNo,
//            @RequestParam(defaultValue = "10") Integer pageSize,
//            @RequestParam(defaultValue = "id") String sortBy,
//            Model model
//    ) {
//        Page<LandslidesNews> page = landslidesNewsService.findAll(pageNo, pageSize, sortBy);
//
//        List<LandslidesNews> list = page.getContent();
//
//        Long totalItems = page.getTotalElements();
//        int totalPages = page.getTotalPages();
//
//        model.addAttribute("newsPage", page);
//
//        return "landslides";
//    }

    @GetMapping("/landslidesnews/{pageNum}")
    public String viewPage(Model model,
                           @PathVariable(name = "pageNum") int pageNum,
                           @RequestParam(required = false) String text,
                           @RequestParam(required = false) String location,
                           @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate localdate,
                           @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate enddate,
                           @RequestParam(required = false) String source) {
        Page<LandslidesNews> page = null;

        Page<Location> pageloc = null;
        if (text != null) {
            // Filter by text only
            page = landslidesNewsService.findByText(text, pageNum - 1, 5, "id");
        } else if(location != null){
            page = landslidesNewsService.findByLocationName(location, pageNum - 1, 5, "id");
        } else if (localdate != null && enddate != null) {
            // Filter by date only
            page = landslidesNewsService.findByDateRange(localdate, enddate, pageNum - 1, 5, "id");
        } else if (source != null) {
            page = landslidesNewsService.findBySource(source, pageNum - 1, 5, "id");
        }
        else {
            // No filters, retrieve all data
            page = landslidesNewsService.findAll(pageNum - 1, 5, "id");
        }

        List<LandslidesNews> list = page.getContent();



        model.addAttribute("currentPage", pageNum);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());
        model.addAttribute("newsPage", list);
        model.addAttribute("localdate", localdate);
        model.addAttribute("enddate", enddate);

        return "landslidesnews";
    }

    @GetMapping("/landslidesnews/edit/{id}")
    public String showEdit(
            @PathVariable String id,
            Model model) {
        LandslidesNews landslidesNews = this.landslidesNewsService.findbyId(id);

        model.addAttribute("landslidesNews", landslidesNews);
        model.addAttribute("locations", locationService.listAll());
        return "landslidesform";
    }

    @PostMapping("/landslidesnews/{id}")
    public String update(@PathVariable String id,
                         @RequestParam String title,
                         @RequestParam List<Long> locationId,
                         @RequestParam String snippet,
                         @RequestParam String source,
                         @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate when) {
        this.landslidesNewsService.edit(id, title, locationId, snippet, source, when);
        return "redirect:/landslidesnews/1";
    }

    @PostMapping("/landslidesnews/delete/{id}")
    public String delete(@PathVariable String id) {
        this.landslidesNewsService.deleteById(id);
        return "redirect:/landslidesnews/1";
    }

}
