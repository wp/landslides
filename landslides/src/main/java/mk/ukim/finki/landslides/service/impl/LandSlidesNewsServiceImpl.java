package mk.ukim.finki.landslides.service.impl;


import lombok.RequiredArgsConstructor;
import mk.ukim.finki.landslides.model.LandslidesNews;
import mk.ukim.finki.landslides.model.Location;
import mk.ukim.finki.landslides.model.exceptions.LandslideNewsNotFoundException;
import mk.ukim.finki.landslides.repository.LandslideNewsRepository;
import mk.ukim.finki.landslides.repository.LocationRepository;
import mk.ukim.finki.landslides.service.LandslidesNewsService;
import mk.ukim.finki.landslides.service.LocationService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class LandSlidesNewsServiceImpl implements LandslidesNewsService {

    private final LandslideNewsRepository repository;
    private final LocationRepository locationRepository;
    private final LocationService locationService;




    @Override
    public Page<LandslidesNews> findAll(Integer pageNo, Integer pageSize, String sortBy) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        return this.repository.findAll(paging);
    }





    @Override
    public Optional<LandslidesNews> save(Map<String, Object> request) {
        String url = request.toString().substring(1, 31);


        Map<String, Object> data = (Map<String, Object>) request.get(url);
        Map<String, Object> ner = (Map<String, Object>) data.get("ner");
        List<String> locList = (List<String>) ner.get("LOC");
        String source = (String) data.get("source");
        String title = (String) data.get("title");
        String when = (String) data.get("when");
        String snippet = (String) data.get("snippet");


        List<Location> locations = new ArrayList<>();
        if (locList != null && !locList.isEmpty()) {
            for (String loc : locList) {
                Location location = this.locationRepository.save(new Location(loc));
                locations.add(location);
            }
        }


        when = when != null ? when.trim() : null;


        LocalDate whenDate = null;
        if (when != null && !when.isEmpty()) {
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            whenDate = LocalDate.parse(when, dateFormatter);
        }


        System.out.println("Source: " + source);
        System.out.println("Title: " + title);
        System.out.println("When: " + when);
        System.out.println("snippet: " + snippet);

        return Optional.of(
                this.repository.save(
                        new LandslidesNews(
                                url,
                                title,
                                locations,
                                snippet,
                                source,
                                whenDate)));
    }

    @Override
    public LandslidesNews save(LandslidesNews landslidesNews) {
        return repository.save(landslidesNews);
    }


    @Override
    public Page<LandslidesNews> findByDateRange(LocalDate startDate, LocalDate endDate, Integer pageNo, Integer pageSize, String sortBy) {
        // Check if the sortBy parameter is 'when', and if not, default to 'id'
        Sort sort = sortBy.equalsIgnoreCase("when") ? Sort.by(sortBy) : Sort.by("id");
        Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
        return repository.findByWhenBetween(startDate, endDate, pageable);
    }



    @Override
    public Page<LandslidesNews> findByText(String searchText, Integer pageNo, Integer pageSize, String sortBy) {
        Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        return repository.findByTitleContainingOrSnippetContaining(searchText, searchText, pageable);
    }

    @Override
    public Page<LandslidesNews> findBySource(String source, Integer pageNo, Integer pageSize, String sortBy) {
        Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        return repository.findBySource(source, pageable);
    }

    @Override
    public LandslidesNews findbyId(String id) {
        return repository.findById(id).orElseThrow(LandslideNewsNotFoundException::new);
    }

    @Override
    public LandslidesNews edit(String id, String title, List<Long> locationId, String snippet, String source, LocalDate when) {
        LandslidesNews landslidesNews = findbyId(id);

        landslidesNews.setTitle(title);
        landslidesNews.setLocation(locationId.stream().map(locationService::findById).collect(Collectors.toList()));
        landslidesNews.setSnippet(snippet);
        landslidesNews.setSource(source);
        landslidesNews.setWhen(when);

        return repository.save(landslidesNews);
    }


    @Override
    public void deleteById(String id) {
        this.repository.deleteById(id);
    }

    public Page<LandslidesNews> findByLocationName(String location, Integer pageNo, Integer pageSize, String sortBy) {
        Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        return repository.findByAllLocationName(location.toLowerCase(), pageable);
    }

}
