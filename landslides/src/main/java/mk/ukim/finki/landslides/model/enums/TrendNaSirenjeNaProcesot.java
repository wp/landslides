package mk.ukim.finki.landslides.model.enums;

public enum TrendNaSirenjeNaProcesot {
    ProgresivnoPoPadinata("Прогресивно (кон нагоре) по падината"),
    ProgresivnoNizPadinata("Прогресивно (кон надолу) низ падината"),
    ProgresivnoBocno("Прогресивно бочно"),
    Smiruvanje("Смирување");

    public final String label;

    TrendNaSirenjeNaProcesot(String label) {
        this.label = label;
    }
}
