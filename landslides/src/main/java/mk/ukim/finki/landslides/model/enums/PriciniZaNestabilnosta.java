package mk.ukim.finki.landslides.model.enums;

public enum PriciniZaNestabilnosta {
        Nepovolni_osobini("Неповолни особини на карпестите маси"),
        Morfometrija("Морфометрија на падината"),

        Erozija("Ерозија во ножица на падина"),
        Sufozija("Суфозија (испирање на ситни фракции"),
        Ogoluvanje("Оголување"),
        Crpenje("Црпење"),
        Staticno_opteretuvanje("Статичко оптеретување на падината"),
        Dinamicki_opteretuvanja("Динамички оптеретувања"),
        Nekontrolirano_ispustanje_voda("Неконтролирано испуштање вода"),
        Iskopi("Ископи, засекувања");

        public final String label;


        PriciniZaNestabilnosta(String label) {
            this.label = label;
        }
}
