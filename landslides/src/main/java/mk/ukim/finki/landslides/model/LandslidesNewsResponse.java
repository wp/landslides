package mk.ukim.finki.landslides.model;

import lombok.Data;

import java.util.List;
@Data
public class LandslidesNewsResponse {
    private List<LandslidesNews> content;
    private int totalPages;
    private long totalElements;

}
