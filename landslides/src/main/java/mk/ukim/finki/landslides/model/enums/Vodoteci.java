package mk.ukim.finki.landslides.model.enums;

public enum Vodoteci {
    Postojani("Постојани"),
    Povremeni("Повремени");

    public final String label;

    Vodoteci(String label) {
        this.label = label;
    }
}
