package mk.ukim.finki.landslides.model.enums;

public enum Pojavi {
    IzdnskoOko("Изднско око"),
    Izvori("Извор/и"),
    Pishtevini("Пиштевина/и"),
    DifuznoPraznenje("Дифузно празнење");

    public final String label;

    Pojavi(String label) {
        this.label = label;
    }
}
